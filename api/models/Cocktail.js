const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CocktailSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    name: {
        type: String,
        required: true
    },
    image: String | null,
    recipe: {
        type: String,
        required: true
    },
    ingredients: [{
        ingredient: {
           type: String,
            required: true
        },
        amount: Number
    }],
    is_published: Boolean,
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;