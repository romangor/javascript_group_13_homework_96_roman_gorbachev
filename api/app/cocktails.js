const express = require('express');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const Cocktail = require("../models/Cocktail");
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');



const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/',async (req, res, next) => {
    try {
        const cocktails = await Cocktail.find();
        res.send(cocktails);
    } catch (e) {
        next(e);
    }
});

router.post('/', auth, permit('admin', 'user'), upload.single('image'),async (req, res, next) => {
    try {
        const ingredients = JSON.parse(req.body.ingredients)
        const cocktailData = {
            user: req.user._id,
            name: req.body.name,
            ingredients: ingredients,
            recipe: req.body.recipe,
            image: req.file ? req.file.filename : null,
            is_published: false
        }
        const cocktail = new Cocktail(cocktailData);
        await cocktail.save();
        res.send(cocktailData);
    } catch (e) {
        next(e);
    }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res, next) => {
    try {
        const cocktail = await Cocktail.findOneAndUpdate({_id: req.body.cocktailId}, {is_published: true});
        res.send(cocktail);
    } catch (e) {
        next(e);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res, next) => {
    try {
        const cocktail = await Cocktail.deleteOne({_id: req.params.id});
        res.send(cocktail);
    } catch (e) {
        next(e);
    }
});




module.exports = router;