const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    mongo: {
        db: 'mongodb://localhost/cocktails',
        options: {useNewUrlParser: true},
    },
    facebook: {
        appId: '397118085751906',
        appSecret: 'a86581e4189367b22ee876f7f97dd5e0'
    }
};