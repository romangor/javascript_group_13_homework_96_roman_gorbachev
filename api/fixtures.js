const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const {nanoid} = require("nanoid");
const Cocktail = require("./models/Cocktail");

const run = async () => {

    await mongoose.connect(config.mongo.db, config.mongo.options);
    const collections = await mongoose.connection.db.listCollections().toArray()

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [Roman, Ivan] = await User.create(
        {
            email: 'roman@mail.com',
            password: '123',
            userName: 'roma',
            avatar: 'roman.jpg',
            role: 'admin',
            token: nanoid(),
            facebookID: '123'
        },
        {
            email: 'ivan@mail.com',
            password: '321',
            userName: 'Vanya',
            avatar: 'ivan.jpg',
            role: 'user',
            token: nanoid(),
            facebookID: '321'
        }
    )
    await Cocktail.create(
        {
            user: Roman,
            name: 'Old Fashioned',
            image: 'oldFashioned.jpg',
            recipe: 'Put sugar in glass. Cover it with dashes of bitters. Add whiskey and stir until sugar dissolves. Add ice, stir again, and serve. If the barman starts shaking the ingredients or muddling fruit, have your next round at another bar.',
            ingredients: [
                {
                    ingredient: 'bourbon or rye whiskey',
                    amount: 2
                },
                {
                    ingredient: 'dashes Angostura bitters',
                    amount: 2
                },
                {
                    ingredient: 'sugar cube or 1 tsp sugar',
                    amount: 1
                },
                {
                    ingredient: 'Orange twist garnish',
                    amount: ''
                }
            ],
            is_published: true,
        },
        {
            user: Ivan,
            name: 'Margarita',
            image: 'margarita.jpg',
            recipe: 'Since this recipe includes fresh juice, it should be shaken. Serve over ice in a glass with a salted rim.',
            ingredients: [
                {
                    ingredient: 'silver tequila',
                    amount: 2
                },
                {
                    ingredient: 'Cointreau',
                    amount: 1
                },
                {
                    ingredient: 'lime juice',
                    amount: 1
                },
                {
                    ingredient: 'Salt for the rim',
                    amount: ''
                }
            ],
            is_published: true,
        },
        {
            user: Roman,
            name: 'Cosmopolitan',
            image: 'cosmopolitan.jpg',
            recipe: 'Build all ingredients in a shaker tine with ice and shake. Strain into a martini glass and garnish with lime wheel or zest.',
            ingredients: [
                {
                    ingredient: 'citrus vodka',
                    amount: 1.5
                },
                {
                    ingredient: 'Cointreau',
                    amount: 1
                },
                {
                    ingredient: 'lime juice',
                    amount: 0.5
                },
                {
                    ingredient: 'cranberry juice',
                    amount: 0.25
                }
            ],
            is_published: false,
        },
        )
    await mongoose.connection.close();
}

run().catch(e => console.error(e))