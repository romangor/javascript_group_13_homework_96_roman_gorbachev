import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { FacebookLoginProvider, SocialAuthService, SocialUser } from 'angularx-social-login';
import { LoginError, LoginUserData, LoginUserFB } from '../../models/User.model';
import { AppState } from '../../store/types';
import { loginUserRequest, loginWithFacebookRequest } from '../../store/users.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit , OnDestroy {
  @ViewChild('f') formLogin!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | LoginError>;
  authStateSub!: Subscription;

  constructor(private store: Store<AppState>, private authService: SocialAuthService) {
    this.loading = store.select(state => state.users.loginLoading);
    this.error = store.select(state => state.users.loginError);
  }
  fbLogin() {
    void this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  };

  ngOnInit(): void {
    this.authStateSub = this.authService.authState.subscribe((user: SocialUser) => {
      const userFb: LoginUserFB = {
        authToken: user.authToken,
        id: user.id,
        email: user.email,
        userName: user.name,
        photoUrl: user.photoUrl,
      };
      this.store.dispatch(loginWithFacebookRequest({userFb}))
    });
  }

  onSubmit() {
    const userData: LoginUserData = this.formLogin.value;
    this.store.dispatch(loginUserRequest({userData}));
  };

  ngOnDestroy(){
    this.authStateSub.unsubscribe();
  }
}
