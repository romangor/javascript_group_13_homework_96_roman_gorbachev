import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Cocktail, cocktailsError } from '../../models/Cocktail.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchCocktailsRequest } from '../../store/cocktails.actions';
import { User } from '../../models/User.model';

@Component({
  selector: 'app-users-cocktails',
  templateUrl: './users-cocktails.component.html',
  styleUrls: ['./users-cocktails.component.sass']
})
export class UsersCocktailsComponent implements OnInit {
  cocktails: Observable<Cocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | cocktailsError>;
  user: Observable<null | User>;

  constructor(private store: Store<AppState>) {
    this.cocktails = store.select(state => state.cocktails.cocktails);
    this.loading = store.select(state => state.cocktails.loadingCocktails);
    this.error = store.select(state => state.cocktails.errorCocktails);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCocktailsRequest())
  }

}
