import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Cocktail, cocktailsError } from '../../models/Cocktail.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { editCocktailRequest, fetchCocktailsRequest, removeCocktailRequest } from 'src/app/store/cocktails.actions';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {
  cocktails: Observable<Cocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | cocktailsError>;

  constructor(private store: Store<AppState>) {
    this.cocktails = store.select(state => state.cocktails.cocktails);
    this.loading = store.select(state => state.cocktails.loadingCocktails);
    this.error = store.select(state => state.cocktails.errorCocktails);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchCocktailsRequest())
  }

  editCocktail(cocktailId: string){
    this.store.dispatch(editCocktailRequest({cocktailId: cocktailId}));
  };

  removeCocktail(cocktailId: string) {
    this.store.dispatch(removeCocktailRequest({cocktailId: cocktailId}));
  };



}
