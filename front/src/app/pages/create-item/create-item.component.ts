import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppState } from 'src/app/store/types';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User } from '../../models/User.model';
import { Router } from '@angular/router';
import { createCocktailRequest } from 'src/app/store/cocktails.actions';
import { cocktailData } from '../../models/Cocktail.model';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.sass']
})
export class CreateItemComponent implements OnInit {
  createCocktailForm!: FormGroup;
  user: Observable<null | User>;
  userData!: User;
  constructor(private store: Store<AppState>, private router: Router) {
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.createCocktailForm = new FormGroup({
      name: new FormControl('', Validators.required),
      ingredients: new FormArray([]),
      recipe: new FormControl('', Validators.required),
      image: new FormControl(),
    });
    this.user.subscribe(user => {
      if(!user) {
       void this.router.navigate(['/']);
      }
      this.userData = user!
    })
  };

  getIngredientsControls() {
    const ingredients = <FormArray>this.createCocktailForm.get('ingredients');
    return ingredients.controls;
  };

  addIngredient() {
    const ingredients = <FormArray>this.createCocktailForm.get('ingredients');
    const ingredientsGroup = new FormGroup({
      ingredient: new FormControl('', Validators.required),
      amount: new FormControl('')
    })
    ingredients.push(ingredientsGroup);
  };

  removeIngredient(i: number) {
    const ingredients = <FormArray>this.createCocktailForm.get('ingredients');
    ingredients.removeAt(i);
  };

  createCocktail() {
    const ingredients = JSON.stringify(this.createCocktailForm.value.ingredients);
    const cocktail: cocktailData = {
      user: this.userData._id,
      name: this.createCocktailForm.value.name,
      ingredients: ingredients,
      image: this.createCocktailForm.value.image,
      recipe: this.createCocktailForm.value.recipe,
    }
  this.store.dispatch(createCocktailRequest({cocktail}))
  }


}
