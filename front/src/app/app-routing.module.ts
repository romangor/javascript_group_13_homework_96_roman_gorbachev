import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './pages/main/main.component';
import { CreateItemComponent } from './pages/create-item/create-item.component';
import { UsersCocktailsComponent } from './pages/users-cocktails/users-cocktails.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', component: MainComponent},
  {path: 'create', component: CreateItemComponent},
  {path: 'userCocktail', component: UsersCocktailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
