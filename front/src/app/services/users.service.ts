import { LoginUserData, LoginUserFB, User } from '../models/User.model';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersServices {
  constructor(private http: HttpClient) {
  }

  loginWithFacebook(userFB: LoginUserFB) {
    return this.http.post<User>(environment.url + '/users/facebookLogin', userFB);
  };

  logout() {
    return this.http.delete(environment.url + '/users/sessions')};

  login(userData: LoginUserData) {
    return this.http.post<User>(environment.url + '/users/sessions', userData);
  };
}
