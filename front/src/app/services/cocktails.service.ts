import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Cocktail, cocktailData } from '../models/Cocktail.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CocktailsService {
  constructor(private http: HttpClient) {
  }

  getCocktails() {
    return this.http.get<Cocktail[]>(environment.url + `/cocktails`)
      .pipe(
        map(response => {
          return response.map(cocktailResponse => {
            return new Cocktail(cocktailResponse.user,
                                cocktailResponse.name,
                                cocktailResponse.recipe,
                                cocktailResponse.image,
                                cocktailResponse.ingredients,
                                cocktailResponse.is_published,
                                cocktailResponse._id);
          });
        })
      )
  };

  createCocktail(cocktail: cocktailData){
      const formData = new FormData();
      formData.append('user', cocktail.user);
      formData.append('name', cocktail.name);
      formData.append('recipe', cocktail.recipe);
      formData.append('ingredients', cocktail.ingredients);
      if (cocktail.image) {
        formData.append('image', cocktail.image);
      }
      return this.http.post(environment.url + '/cocktails', formData);
  }

  editCocktail(cocktailId: string) {
    return this.http.post(`${environment.url}/cocktails/${cocktailId}/publish`, {cocktailId: cocktailId});
  }

  removeCocktail(cocktailId: string) {
    return this.http.delete(`${environment.url}/cocktails/${cocktailId}`);
  }

}
