import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { LoginComponent } from './pages/login/login.component';
import { CenteredCardComponent } from './ui/centered-card/centered-card.component';
import { MainComponent } from './pages/main/main.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { CreateItemComponent } from './pages/create-item/create-item.component';
import { MatMenuModule } from '@angular/material/menu';
import { FlexModule } from '@angular/flex-layout';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './auth.interceptor';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { environment } from '../environments/environment';
import { localStorageSync } from 'ngrx-store-localstorage';
import { usersReducer } from './store/users.reducer';
import { UsersEffects } from './store/users.effects';
import { UserTypeDirective } from './directives/user-type.derective';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CocktailsEffects } from './store/cocktails.effects';
import { cocktailsReducer } from './store/cocktails.reducer';
import { HasRolesDirective } from './directives/has-roles.directive';
import { UsersCocktailsComponent } from './pages/users-cocktails/users-cocktails.component';
import { UserCocktailChek } from './directives/user-cocktail-chek';

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(environment.facebookID, {
        scope: 'email,public_profile',
      })
    }
  ]
};

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{users: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];


@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent,
    CenteredCardComponent,
    MainComponent,
    FileInputComponent,
    CreateItemComponent,
    UserTypeDirective,
    HasRolesDirective,
    UsersCocktailsComponent,
    UserCocktailChek
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatMenuModule,
    FlexModule,
    HttpClientModule,
    MatSnackBarModule,
    SocialLoginModule,
    StoreModule.forRoot({users: usersReducer, cocktails: cocktailsReducer}, {metaReducers}),
    EffectsModule.forRoot([UsersEffects, CocktailsEffects])
  ],
  providers: [{provide: 'SocialAuthServiceConfig', useValue: socialConfig},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
