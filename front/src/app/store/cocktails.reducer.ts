import { CocktailsState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createCocktailFailure,
  createCocktailRequest, createCocktailSuccess,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess
} from './cocktails.actions';



const initialState: CocktailsState = {
  cocktails: [],
  loadingCocktails: false,
  errorCocktails: null,
  createLoading: false,
  createError: null

};

export const cocktailsReducer = createReducer(
  initialState,
  on(fetchCocktailsRequest, state => ({
    ...state,
    loadingCocktails: true,
    errorCocktails: null,
  })),
  on(fetchCocktailsSuccess, (state, {cocktails}) => ({
    ...state,
    loadingCocktails: false,
    cocktails
  })),
  on(fetchCocktailsFailure, (state, {error}) => ({
    ...state,
    loadingCocktails: false,
    errorCocktails: error
  })),

  on(createCocktailRequest, state => ({...state, createLoading: true})),
  on(createCocktailSuccess, state => ({...state, createLoading: false})),
  on(createCocktailFailure, (state, {error}) => ({...state, createLoading: false, createError: error,})),
)
