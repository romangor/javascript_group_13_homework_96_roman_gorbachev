import { createAction, props } from '@ngrx/store';
import { LoginError, LoginUserData, LoginUserFB, User } from '../models/User.model';

export const loginWithFacebookRequest = createAction('[User] LoginFb Request', props<{userFb: LoginUserFB}>());
export const loginWithFacebookSuccess = createAction('[User] LoginFB Successful',props<{user: User}>());
export const loginWithFacebookFailure = createAction('[User] LoginFB Failure',props<{error: null | LoginError}>());

export const loginUserRequest = createAction(
  '[Users] Login Request',
  props<{userData: LoginUserData}>()
);
export const loginUserSuccess = createAction(
  '[Users] Login Success',
  props<{user: User}>()
);
export const loginUserFailure = createAction(
  '[Users] Login Failure',
  props<{error: null | LoginError}>()
);

export const logoutUser = createAction('[Users] Logout');
export const logoutUserRequest = createAction('[Users] Server Logout Request');
