import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { HelpersService } from '../services/helper.service';
import { CocktailsService } from '../services/cocktails.service';
import {
  createCocktailFailure,
  createCocktailRequest, createCocktailSuccess, editCocktailFailure, editCocktailRequest, editCocktailSuccess,
  fetchCocktailsRequest,
  fetchCocktailsSuccess, removeCocktailFailure, removeCocktailRequest, removeCocktailSuccess
} from './cocktails.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { loginWithFacebookFailure } from './users.actions';
import { Store } from '@ngrx/store';
import { AppState } from './types';


@Injectable()
export class CocktailsEffects {
  constructor(  private actions: Actions,
                private cocktailsService: CocktailsService,
                private helpers: HelpersService,
                private router: Router,
                private store: Store<AppState>) {}

  fetchCocktails = createEffect( () => this.actions.pipe(
    ofType(fetchCocktailsRequest),
    mergeMap(() => this.cocktailsService.getCocktails().pipe(
        map((cocktails) => fetchCocktailsSuccess({cocktails})),
      this.helpers.catchServerError(loginWithFacebookFailure)
      )
    )));

  createCocktail = createEffect( () => this.actions.pipe(
    ofType(createCocktailRequest),
    mergeMap( ({cocktail}) => this.cocktailsService.createCocktail(cocktail).pipe(
      map( () => createCocktailSuccess()),
      tap( () => {
        this.helpers.openSnackbar('Cocktail created successfully');
        void this.router.navigate(['/']);
      }),
      catchError( () => of( createCocktailFailure({error: 'Error creating cocktail'})))
    ))
  ));

  editCocktail = createEffect(() => this.actions.pipe(
    ofType(editCocktailRequest),
    mergeMap(({cocktailId}) => this.cocktailsService.editCocktail(cocktailId).pipe(
      map( () => editCocktailSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Cocktail published successfully');
        this.store.dispatch(fetchCocktailsRequest());
      }),
      this.helpers.catchServerError(editCocktailFailure)
    ))));

  removeCocktail= createEffect(() => this.actions.pipe(
    ofType(removeCocktailRequest),
    mergeMap(({cocktailId}) => this.cocktailsService.removeCocktail(cocktailId).pipe(
      map( () => removeCocktailSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Cocktail removed');
        this.store.dispatch(fetchCocktailsRequest());
      }),
      this.helpers.catchServerError(removeCocktailFailure)
    ))));


}
