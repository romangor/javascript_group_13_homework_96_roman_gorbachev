import { createAction, props } from '@ngrx/store';
import { Cocktail, cocktailData, cocktailsError } from '../models/Cocktail.model';

export const fetchCocktailsRequest = createAction('[Cocktails] Fetch Cocktails Request');
export const fetchCocktailsSuccess = createAction('[Cocktails] Fetch Cocktails Success', props<{cocktails: Cocktail[]}>());
export const fetchCocktailsFailure = createAction('[Cocktails] Fetch Cocktails Failure', props<{error: cocktailsError}>());

export  const createCocktailRequest = createAction('[Cocktail] Create Request', props<{cocktail: cocktailData}>());
export  const createCocktailSuccess = createAction('[Cocktail] Create Success');
export  const createCocktailFailure = createAction('[Cocktail] Create Failure', props<{error: string}>());

export const editCocktailRequest = createAction('[Cocktail] Edit Request', props<{cocktailId: string}>());
export const editCocktailSuccess = createAction('[Cocktail] Edit Success');
export const editCocktailFailure = createAction('[Cocktail] Edit Failure', props<{error: string}>());

export const removeCocktailRequest = createAction('[Cocktail] Remove Request', props<{cocktailId: string}>());
export const removeCocktailSuccess = createAction('[Cocktail] Remove Success');
export const removeCocktailFailure = createAction('[Cocktail] Remove Failure', props<{error: string}>());

