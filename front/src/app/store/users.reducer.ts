import { createReducer, on } from '@ngrx/store';
import { UsersState } from './types';
import {
  loginUserFailure,
  loginUserRequest, loginUserSuccess,
  loginWithFacebookFailure,
  loginWithFacebookRequest,
  loginWithFacebookSuccess,
  logoutUser,
} from './users.actions';

const initialState: UsersState = {
  user: null,
  loginLoading: false,
  loginError: null,
};

export const usersReducer = createReducer(
  initialState,
  on(loginWithFacebookRequest, state => ({
    ...state,
    loginLoading: true,
    loginError: null,
  })),
  on(loginWithFacebookSuccess, (state, {user}) => ({
    ...state,
    loginLoading: false,
    user
  })),
  on(loginWithFacebookFailure, (state, {error}) => ({
    ...state,
    loginLoading: false,
    loginError: error
  })),

  on(logoutUser, state => ({
    ...state,
    user: null,
  })),
  on(loginUserRequest, state => ({
    ...state,
    loginLoading: true,
    loginError: null,
  })),
  on(loginUserSuccess, (state, {user}) => ({
    ...state,
    loginLoading: false,
    user
  })),
  on(loginUserFailure, (state, {error}) => ({
    ...state,
    loginLoading: false,
    loginError: error
  })),
)


