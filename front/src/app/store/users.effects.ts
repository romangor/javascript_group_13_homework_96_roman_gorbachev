import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import {
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess,
  loginWithFacebookFailure,
  loginWithFacebookRequest,
  loginWithFacebookSuccess,
  logoutUser,
  logoutUserRequest,
} from './users.actions';
import { mergeMap, tap } from 'rxjs';
import { map } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UsersServices } from '../services/users.service';
import { HelpersService } from '../services/helper.service';
import { SocialAuthService } from 'angularx-social-login';


@Injectable()
export class UsersEffects {
  constructor(
    private actions: Actions,
    private usersService: UsersServices,
    private router: Router,
    private snackbar: MatSnackBar,
    private helpers: HelpersService,
    private auth: SocialAuthService,
  ){}

  loginUserFB = createEffect(() => this.actions.pipe(
    ofType(loginWithFacebookRequest),
    mergeMap(({userFb}) => this.usersService.loginWithFacebook(userFb).pipe(
      map(user => loginWithFacebookSuccess({user})),
      tap(() => {
        this.helpers.openSnackbar('Login successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(loginWithFacebookFailure)
    ))
  ));

  logoutUser = createEffect(() => this.actions.pipe(
    ofType(logoutUserRequest),
    mergeMap(() => {
      return this.usersService.logout().pipe(
        map(() => logoutUser()),
        tap(() => {
          void this.auth.signOut();
          void this.router.navigate(['/']);
          this.helpers.openSnackbar('Logout successful');
        })
      );
    }))
  );

  loginUser = createEffect(() => this.actions.pipe(
    ofType(loginUserRequest),
    mergeMap(({userData}) => this.usersService.login(userData).pipe(
      map(user => loginUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackbar('Login successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(loginUserFailure)
    ))
  ));
}

