import { LoginError, User } from '../models/User.model';
import { Cocktail, cocktailsError } from '../models/Cocktail.model';

export type UsersState ={
  user: User | null,
  loginLoading: boolean,
  loginError: null | LoginError,
}

export type CocktailsState = {
  cocktails: Cocktail[],
  createLoading: boolean,
  createError: null | string,
  loadingCocktails: boolean,
  errorCocktails: null | cocktailsError
}

export type AppState = {
  users: UsersState,
  cocktails: CocktailsState
}
