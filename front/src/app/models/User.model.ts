export interface User {
  _id: string,
  email: string,
  token: string,
  avatar: string | null,
  userName: string,
  role: string
}


export interface FieldError {
  message: string
}

export interface LoginUserData {
  email: string,
  password: string,
}

export interface LoginUserFB {
  authToken: string,
  id: string,
  email: string,
  userName: string,
  photoUrl: string
}

export interface LoginError {
  error: FieldError
}
