export class Cocktail {
  constructor(
    public user: string,
    public name: string,
    public recipe: string,
    public image: string | null,
    public ingredients: [{
      ingredient: string,
      amount: number
    }],
    public is_published: boolean,
    public _id: string
  ) {
  }
}

export interface cocktailData {
  user: string,
  name: string,
  recipe: string,
  image: File | null,
  ingredients: string,
}

export interface cocktailsError {
  message: string
}

